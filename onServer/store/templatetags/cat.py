from django import template
from store.models import Category
from carton.cart import Cart

register = template.Library()

@register.inclusion_tag('andre.html')
def cat():
     category = Category.objects.exclude(name="Glass").exclude(name="Tekstil").exclude(name="Bok")
     return {'catogery': category}

@register.inclusion_tag('total.html')
def get_total():
    cart = Cart(request.session)
    total = cart.total
    return {'cost': cost}
