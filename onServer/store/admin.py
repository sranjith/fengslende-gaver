from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin
from .models import Category, Product, Image

# Register your models here.
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug': ('name',)}
admin.site.register(Category, CategoryAdmin)

class ImageInline(admin.StackedInline):
    model = Image
    extra = 1

class ProductAdmin(SummernoteModelAdmin):
    list_display = ['name', 'slug', 'category', 'price', 'stock', 'solgt', 'available', 'salg', 'updated']
    list_filter = ['available', 'updated', 'category', 'salg', 'solgt']
    list_editable = ['price', 'stock', 'available', 'salg']
    prepopulated_fields = {'slug': ('name',)}
    summer_note_fields = '__all__'
    inlines = [ImageInline]
admin.site.register(Product, ProductAdmin)
