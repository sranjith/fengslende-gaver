# -*- coding: utf-8 -*-
from django.db import models
from django.core.urlresolvers import reverse
import sys

reload(sys)
sys.setdefaultencoding('utf8')


# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=40, db_index=True)
    slug = models.SlugField(max_length=40, db_index=True, unique=True)

    class Meta:
        ordering = ('name',)
        verbose_name = 'Kategori'
        verbose_name_plural = 'Kategorier'

    def __str__(self):
        return self.name.encode('utf8')

    def get_absolute_url(self):
        return reverse('shop:product_list_by_category', args=[self.slug])

class Product(models.Model):
    category = models.ForeignKey(Category, related_name='product')
    name = models.CharField(max_length=40)
    slug = models.SlugField(max_length=40, db_index=True, unique=True)
    image = models.ImageField(upload_to='product/%Y/%m/%d', blank=False)
    description = models.TextField(blank=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    stock = models.PositiveIntegerField()
    solgt = models.PositiveIntegerField(default=0)
    available = models.BooleanField(default=True)
    salg = models.BooleanField(default=False)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-updated',)
        index_together = (('id','slug'),)
        verbose_name = 'Produkt'
        verbose_name_plural = 'Produkter'

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name.encode('utf-8')

    def get_absolute_url(self):
        return reverse('shop:product_detail', args=[self.id, self.slug])

    def get_cart_url(self):
        return reverse('shop:add', args=[self.id, self.slug])

class Image(models.Model):
    name = models.CharField(max_length=40)
    image = models.ImageField(upload_to='product/%Y/%m/%d', blank=False)
    product = models.ForeignKey(Product, related_name="bilder")

    def __str__(self):
        return self.name.encode('utf8')

    class Meta:
        verbose_name = 'Bilde'
        verbose_name_plural = 'Bilder'
