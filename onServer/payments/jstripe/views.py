from django.shortcuts import render, HttpResponse
from .forms import OrderForm
from store.models import Product
import stripe
from carton.cart import Cart

stripe.api_key = 'sk_live_mCzZJGhishO3nszTbYWgy7oG'
def payment(request):
    cart = Cart(request.session)
    total = cart.total

    if request.method == 'POST':
        ting = ''
        for item in cart.items:
            ting = ting+'\n'+str(item.quantity)+' av '+item.product.name

        mutable = request.POST._mutable
        request.POST._mutable = True
        request.POST['beskrivelse']= ting
        request.POST._mutable = mutable

        form = OrderForm(request.POST)

        if form.is_valid():
            token = form.cleaned_data['token']

            try:
                stripe.Charge.create(
                    amount = int(total*100),
                    currency = "nok",
                    description = ""+form.cleaned_data['etternavn']+", "+form.cleaned_data['fornavn'],
                    source = token,
                    )
                for item in cart.items:
                    vare = Product.objects.get(id=item.product.id)
                    if vare.stock >= item.quantity:
                        vare.stock -= item.quantity
                    else:
                        vare.stock = 0
                    vare.solgt += item.quantity
                    vare.save()

                form.save()
                args = {}
                args['fornavn'] = form.cleaned_data['fornavn']
                args['etternavn'] = form.cleaned_data['etternavn']
                args['addresse'] = form.cleaned_data['addresse']
                args['postnummer'] = form.cleaned_data['postnummer']
                args['poststed'] = form.cleaned_data['poststed']
                cart.clear()
                return render(request, 'virk.html', args)

            except stripe.CardError as e:
                form.add_error("Kortet ble ikke akseptert")
    else:
        form = OrderForm(request.POST)

    return render(request, 'stripe.html', {'form':form})
