from django.contrib import admin
from .models import Order

# Register your models here.

class OrderAdmin(admin.ModelAdmin):
    list_display_links = None
    list_display = ['sendt', 'addresse', 'fornavn', 'etternavn', 'postnummer', 'poststed']
    list_filter = ['sendt']
    list_editable = ['sendt']

admin.site.register(Order, OrderAdmin)
