# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-01-22 13:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0002_auto_20180122_1359'),
    ]

    operations = [
        migrations.AddField(
            model_name='slideshow',
            name='storrelse',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='størrelse av bilde(mellom 0 og 100)'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='slideshow',
            name='x',
            field=models.SmallIntegerField(blank=True, default=0, verbose_name='vertikal av bilde (mellom -100 og 100)'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='slideshow',
            name='y',
            field=models.SmallIntegerField(blank=True, default=9, verbose_name='horisontal av bilde (mellom -100 og 100)'),
            preserve_default=False,
        ),
    ]
