# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0005_auto_20180122_1419'),
    ]

    operations = [
        migrations.AlterField(
            model_name='slideshow',
            name='storrelse',
            field=models.PositiveSmallIntegerField(default=100, verbose_name=b'st\xc3\xb8rrelse av bilde(mellom 0 og 100)'),
        ),
    ]
