from django.db import models
from django.core.urlresolvers import reverse

# Create your models here.
class BlogArticle(models.Model):
    tittel = models.CharField(max_length=40)
    slug = models.SlugField(max_length=40, db_index=True, unique=True)
    bilde = models.ImageField(upload_to='blog/%Y/%m/%d', blank=False)
    ingress = models.TextField(max_length=100, blank=True)
    hoveddel = models.TextField(max_length=80000, blank=True)
    laget = models.DateTimeField('Datoen den kommer ut')

    class Meta:
        ordering = ('-laget',)
        index_together = (('id','slug'),)
        verbose_name = 'Blogg Artikkel'
        verbose_name_plural = 'Blogg Artikkler'

    def __str__(self):
        return self.tittel

    def get_absolute_url(self):
        return reverse('blog:blog_list', args=[self.id, self.slug])
