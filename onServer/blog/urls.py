from django.conf.urls import url, include
from . import views


urlpatterns = [
    url(r'^$', views.blogger, name='alle_blogger'),
    url(r'^(?P<id>\d+)/(?P<slug>[-\w]+)/$', views.blog_list, name='blog_list')
]
