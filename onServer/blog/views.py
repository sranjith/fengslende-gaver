from django.shortcuts import render, get_object_or_404
from .models import BlogArticle
from datetime import datetime

# Create your views here.
def blogger(request):
    artikkler = BlogArticle.objects.all()
    artikkel_vis = artikkler.filter(laget__lt=datetime.now())
    return render(request, 'blogger.html', {'blogger':artikkel_vis})

def blog_list(request, id, slug):
    blogg = get_object_or_404(BlogArticle, id=id, slug=slug)
    return render(request, 'blog_list.html', {'blogg':blogg,})
