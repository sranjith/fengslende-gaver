from .models import Order
from django.forms import ModelForm
from django.core.exceptions import NON_FIELD_ERRORS

class OrderForm(ModelForm):


    class Meta:
        model = Order
        fields = ('fornavn', 'etternavn','epost', 'addresse','postnummer','poststed', 'token', 'beskrivelse', 'sendt')

    def add_error(self, message, *args):
        self._errors[NON_FIELD_ERRORS] = self.error_class([message])
