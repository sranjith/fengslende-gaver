from django.db import models
from django.core.urlresolvers import reverse

# Create your models here.
class Order(models.Model):
    sendt = models.BooleanField(default=False)
    fornavn = models.CharField(max_length=100)
    etternavn = models.CharField(max_length=100)
    epost = models.EmailField(max_length=254)
    addresse = models.CharField(max_length=200)
    postnummer = models.CharField(max_length=4)
    poststed = models.CharField(max_length=100)
    token = models.CharField(max_length=200)
    beskrivelse = models.TextField(max_length=8000, blank=True)
    laget = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.addresse

    class Meta:
        ordering = ('-laget',)

        verbose_name = 'betaling'
        verbose_name_plural = 'betalinger'
