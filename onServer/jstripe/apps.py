from django.apps import AppConfig


class JstripeConfig(AppConfig):
    name = 'jstripe'
