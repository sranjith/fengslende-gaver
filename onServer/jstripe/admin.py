from django.contrib import admin
from .models import Order

# Register your models here.

class OrderAdmin(admin.ModelAdmin):
    list_display = ['addresse', 'sendt' , 'fornavn', 'etternavn', 'postnummer', 'poststed']
    list_filter = ['sendt']
    list_editable = ['sendt']
    readonly_fields=('beskrivelse','token', 'addresse', 'fornavn', 'etternavn', 'postnummer', 'poststed')

admin.site.register(Order, OrderAdmin)
