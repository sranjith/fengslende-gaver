var antall = $( "#slideshow" ).children().length - 2;
var slide = 0;
var bredde = $( "#slideshow" ).width();
var defualttime = 3;
var time = defualttime;

function createSilde (antall) {
  $( ".slide:eq(0)" ).addClass( "aktiv" );
  $( ".slide:eq(0)" ).css("left", ""+0+"px");

  for (var i = 1; i < antall; i++) {
    $( ".slide:eq("+i+")" ).css("left", ""+bredde+"px");
  }
}

function changeSlide (antall, retning) {
  var time = 500;
  var pre = slide;


  $( ".slide:eq("+(pre)+")" ).animate({"left": ""+((-1)*bredde*retning)+"px"}, {
    duration:time,
    queue: false
  }).removeClass( "aktiv" );

  if (pre+1 == antall && retning == 1) {
    pre = -1;
    slide = -1;
    $( ".slide:eq("+(pre+1*retning)+")" ).addClass( "aktiv" ).animate({"left": "0px"}, {
      duration:time,
      queue: false
    });

    $( ".slide:eq(0)" ).css("left", ""+0+"px");

    for (var i = 1; i < antall; i++) {
      $( ".slide:eq("+i+")" ).animate({"left": ""+bredde+"px"}, {
        duration:time,
        queue: false
      }).removeClass( "aktiv" );
    }
  }

  else if (pre==0 && retning==-1) {
    $( ".slide:eq("+(antall-1)+")" ).addClass( "aktiv" ).animate({"left": "0px"}, {
      duration:time,
      queue: false
    });

    for (var i = 0; i < antall-1; i++) {
      $( ".slide:eq("+i+")" ).animate({"left": ""+(-1)*bredde+"px"}, {
        duration:time,
        queue: false
      }).removeClass( "aktiv" );
    }
    slide = antall;

  }
  else {
    $( ".slide:eq("+(pre+1*retning)+")" ).addClass( "aktiv" ).animate({"left": "0px"}, {
      duration:time,
      queue: false
    });
  }


  slide+= retning;
}



function slideButton (retning) {
  changeSlide(antall, retning);
  $( ".arrow" ).blur();
  time = defualttime;
}

function slideWidth () {
  bredde = $( "#slideshow" ).width();
  for (var i = 0; i < antall-1; i++) {
    if(i!=slide) {
      $( ".slide:eq("+i+")" ).animate({"left": ""+bredde+"px"}, {
        duration:time,
        queue: false
      }).removeClass( "aktiv" );
    }
  }
}

function autoSlide () {
    time -= 1;

    if(time == 0) {
      time = defualttime;
      changeSlide(antall, 1);
    }
}


//------------------------------------------------Document starter her ------------------------------------------//
$(document).ready(function() {
  createSilde(antall);

  window.setInterval(function(){
    autoSlide();
  }, 2000);

  $(window).on('resize', function (){
    slideWidth();
  });

  $(window).resize(function (){
    slideWidth();
  });
});
