Stripe.setPublishableKey('pk_live_TkGF7yuyMEhFvzdwaNY8gPF9');

$( '#payment-form' ).submit(function(e){
  $form = $(this);

  $form.find('button').prop('disabled', true);

  Stripe.createToken($form, function(status, response){
      if(response.error){
        $form.find('.payment-errors').text(response.error.message);
        $form.find('button').prop('disabled', false);
      }
      else{
        var token = response.id;

        $form.find('input[id="id_token"]').val(token);
        $form.get(0).submit();
      }
  });
  return false;
});

$('.alert strong').each(function() {
    var text = $(this).text();
    $(this).text(text.replace('None', ' '));
});
