from django.db import models
from django.core.urlresolvers import reverse
from decimal import Decimal

from carton.cart import Cart

from store.models import Product

class Slideshow(models.Model):
    produkt = models.ForeignKey(Product, related_name='product')
    navn = models.CharField(max_length=40)
    rangering = models.PositiveSmallIntegerField()
    dato = models.DateTimeField('Datoen den kommer ut')
    storrelse = models.PositiveSmallIntegerField('størrelse av bilde(mellom 0 og 100)', default=100)
    x = models.SmallIntegerField('vertikal av bilde (mellom -100 og 100)', blank = True, default=50)
    y = models.SmallIntegerField('horisontal av bilde (mellom -100 og 100)', blank = True, default=50)

    class Meta:
        ordering = ('rangering', '-dato',)
        verbose_name = "Slideshow"
        verbose_name_plural = "Slideshow"

    def __str__(self):
        return self.navn
