from django.shortcuts import render, redirect
from datetime import datetime
from store.models import Category, Product
from blog.models import BlogArticle
from .models import Slideshow
from carton.cart import Cart
from django.http import HttpResponse

# Create your views here.
def home(request):
    slideshow = Slideshow.objects.all().filter(dato__lt=datetime.now())
    blogg = BlogArticle.objects.all().filter(laget__lt=datetime.now())[:3]
    return render(request, 'index.html', {'slide':slideshow, 'blogg':blogg})

def show(request):
    cart = Cart(request.session)
    total = cart.total
    return render(request, 'cart.html', {'total': total,})

def add_post(request):
    cart = Cart(request.session)
    product = Product.objects.get(id=request.GET.get('product_id'))
    cart.add(product, price=product.price)
    return HttpResponse("Added")

def remove_post(request):
    cart = Cart(request.session)
    product = Product.objects.get(id=request.GET.get('product_id'))
    cart.remove(product)
    return redirect('home:show')
