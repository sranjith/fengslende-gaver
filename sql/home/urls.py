from django.conf.urls import url, include
from django.views.generic import ListView, DetailView
from . import views

app_name = 'home'
urlpatterns = [
    url(r'^$', views.home, name='home_page'),
    url(r'^handlekurv/$', views.show, name='show'),
    url(r'^remove/$', views.remove_post, name='remove'),
    url(r'add_post/$', views.add_post, name='add_post'),
    ]
