from django.contrib import admin
from .models import Slideshow

# Register your models here.
class SlideAdmin(admin.ModelAdmin):
    list_display = ['navn','rangering', 'produkt', 'dato']
    list_filter = ['rangering', 'dato']
    list_editable = ['rangering', 'dato']
    prepopulated_fields = {'navn': ('produkt',)}
admin.site.register(Slideshow, SlideAdmin)
