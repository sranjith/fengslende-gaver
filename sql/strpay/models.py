from django.db import models

# Create your models here.
class Order(models.Model):
    fornavn = models.CharField(max_length=40)
    etternavn = models.CharField(max_length=40)
    addresse = models.CharField(max_length=40)
    postnummer = models.PositiveIntegerField()
    poststed = models.CharField(max_length=40)
    email = models.EmailField()
    sendt = models.BooleanField(default=False)
    varer = models.CharField(max_length=800)

    def __str__(self):
        return self.addresse
