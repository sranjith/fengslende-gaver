from django.apps import AppConfig


class StrpayConfig(AppConfig):
    name = 'strpay'
