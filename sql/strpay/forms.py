from .models import Order
from django.forms import ModelForm
from django.core.exceptions import NON_FIELD_ERRORS

class OrderForm(ModelForm):


    class Meta:
        model = Order
        fields = ('fornavn', 'etternavn', 'email','addresse','postnummer','poststed', 'stripe_id', 'sendt','varer')

    def add_error(self, message):
        self._errors[NON_FIELD_ERRORS] = self.error_class([message])
