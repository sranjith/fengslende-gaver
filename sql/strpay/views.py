from django.shortcuts import render, redirect
import stripe
import datetime
from .forms import OrderForm
from carton.cart import Cart
# Create your views here.

stripe.api_key = settings.STRIPE_SECRET

def register(request):
    cart = Cart(request.session)
    total = cart.total
    if request.method == 'POST':
        form = OrderForm(request.POST)
        if form.is_valid():
            try:
                customer = stripe.Charge.create(
                    amount = total*100,
                    currency = 'NOK',
                    description = form.cleaned_data['email'],
                    card = form.cleaned_data['stripe_id']
                )

                form.save()
                redirect('/regiser_success')

            except stripe.CardError:
                form.add_error("The card has been declined")

    else:
        form = CustomUserForm()

    args = {}
    args.update(csrf(request))
    args['form'] = form
    args['publishable'] = settings.STRIPE_PUBLISHABLE
    args['months'] = range (1,12)
    args['years'] = range(2018, 2038)
    args['soon'] = datetime.date.today() + datetime.timedelta(days=30)
    args['total'] = total
    render(request, 'register.html', args)
