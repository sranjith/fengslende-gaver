from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse

from .models import Category, Product, Image
from carton.cart import Cart

# Create your views here.
def product_list(request, category_slug=None):
    category= None
    categories = Category.objects.all()
    products = Product.objects.filter(available=True)
    if category_slug:
        category = get_object_or_404(Category, slug=category_slug)
        products = products.filter(category=category)
    return render(request, 'store.html', {'category': category,
                                          'categories': categories,
                                          'products': products})

def product_detail(request, id, slug):
    product = get_object_or_404(Product, id=id, slug=slug, available=True)
    image = Image.objects.filter(product=product)
    #cart_product_form = CartAddProductForm()
    return render(request, 'store-detail.html', {'product':product,'image':image})#'cart_product_form': cart_product_form})

def add(request, id, slug):
    cart = Cart(request.session)
    product = get_object_or_404(Product, id=id, slug=slug, available=True)
    cart.add(product, price=product.price)
    return redirect(product)

def show(request):
    cart = Cart(request.session)
    total = cart.total
    return render(request, 'cart.html', {'total': total,})
