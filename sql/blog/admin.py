from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin
from .models import BlogArticle

# Register your models here.
class LagBlogg(SummernoteModelAdmin):
    list_display = ['tittel', 'slug', 'laget']
    list_filter = ['laget']
    prepopulated_fields = {'slug': ('tittel',)}
    summer_note_fields = '__all__'

admin.site.register(BlogArticle, LagBlogg)
